"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import os

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.controls
control_list = db.samples
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    flask.session['data_entries'] = 0
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/brevet_times")
def brevet_page():
    app.logger.debug("Page for displaying values in mongoDB")
    #create list of values via mongoDB
    return #literal template for time display
            #How do I send list of data to html page? Variables of one list? While loop?

################
#
# AJAX DB handlers
#   for entering/returning data for mongoDB
#################
@app.route("/_display_values")
def _display_values():
    global control_list
    if not flask.session['data_entries']:
        return flask.render_template('calc.html')
    else:
        result = list(control_list.find())
        result = str(result)
        flask.session['data_entries'] = 0
        db.samples.drop()
        control_list = db.samples
        return flask.render_template('display.html', db_data=result)#Call a new template that displays mongoDB's information

@app.route("/_update_values")
def _update_values():
    #mongo add/append to database commands
    #create structure for distance, time, total distance, open, close
    #code below is copy of display code. Replace data setting with
    #mongo append and addition commands. Add a mongo initializer
    #for the database within the index page response
    app.logger.debug("Got a JSON request")
    km = str(request.args.get('km', type = int))
    open_time = request.args.get('brevet_open', type = str)
    close_time = request.args.get('brevet_close', type = str)
    result = "Data Unchanged"
    if open_time == "Proper Time" or open_time == "Positive Integer" or close_time == "Proper Time" or close_time =="Positive Integer":
        return flask.jsonify(result="improper fields in data, please properly fill them in.")
    if open_time and close_time:
        if control_list.find_one({km: km, open_time: open_time, close_time: close_time}):
            result = "Data already exists. Please enter new data"
        else:
            control_list.insert({km: km, open_time: open_time, close_time: close_time})
            app.logger.debug("got a db update: " + str(control_list.find_one({km: km, open_time: open_time})))
        if control_list.find_one({km: km, open_time: open_time, close_time: close_time}):
            result = "Data properly inserted into database"
            flask.session['data_entries'] = +1
        else:
            result = "Data improperly inserted"
        #remove data that possibly got inserted, display full database.
    return flask.jsonify(result=result)



###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type = float)
    total_distance = request.args.get('brevet_distance', type = int)
    start_time = request.args.get('timestamp', type = str)
    start_time = arrow.get(start_time)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, total_distance, start_time.isoformat())
    close_time = acp_times.close_time(km, total_distance, start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
