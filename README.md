# Project 5: Brevet time calculator with Ajax and MongoDB

A simple web application using MongoDB, Flask and AJAX to allow inputs of calculated times
and display them. Website will take in time and date, provide a processed and formated 
output of the brevet open/close times. Click submit will add the current fields to the database.
Clicking display will load a page with the information in the data. It will than reset the database
and a button will send you back to the start.

## What is in this repository

A few manual tests were used to complete this application;
Clicking when no fields are entered: does not send any information
Click when all or few fields are entered: Only sends properly filled areas
Attempting to provide incorrect data and clicking: N/A
clicking several times: Repeated fields are not added again to database

## Author
Isaac Perks: iperks@uoregon.edu
Fork: UOCIS 322 proj5
